## Introduction


Third-party customized FreeBSD images. 


## Release 


### 13.0 

Bootable Image with textmode = 0, with debug 16, and with vbe 0x17e, compatible and tested.
It contains the classic FreeBSD installer. Size of img file: 832MB.
**Compatible with EEEPC ASUS, Intel.**


````
  qemu-system-i386   -machine type=pc  -drive file=vdisk.qcow2,index=0,media=disk,format=qcow2  -drive file=FreeBSD-13.0-RELEASE-i386-memstick.img,index=1,media=disk,format=raw   -boot c  
````


https://gitlab.com/openbsd98324/cmfreebsd/-/raw/main/pub/freebsd/13.0/i386/FreeBSD-13.0-RELEASE-i386-memstick-custom-vesa-1645260986-v1.img.gz
